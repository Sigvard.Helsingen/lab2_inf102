package INF102.lab2.list;


import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ExecutionException;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;

	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) {
		if (index < 0 || index > elements.length){
			throw new IndexOutOfBoundsException("Index " + index + " is out of bounds");
		}
		else if (elements[index] == null){
			throw new IndexOutOfBoundsException("Index " + index + " is out of bounds");
		}
		else {return (T) elements[index];}
	}
	
	@Override
	public void add(int index, T element) {

		if (n == elements.length){
			int biggerArrayList = elements.length * 2;
			elements = Arrays.copyOf(elements, biggerArrayList);
		}
		for (int i = n; i > index; i--){
			elements[i] = elements[i - 1];
		}
		elements[index] = element;
		n++;

	}

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}